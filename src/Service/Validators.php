<?php

namespace Drupal\laravel_helpers\Service;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Illuminate\Translation\ArrayLoader;
use Illuminate\Translation\Translator;
use Illuminate\Validation\Factory as ValidatorFactory;

/**
 * The Laravel validator service.
 */
class Validators implements ValidatorsInterface {

  /**
   * The Laravel validator.
   */
  protected ValidatorFactory $validator;

  /**
   * The module extension list.
   */
  protected ModuleExtensionList $extensionList;

  /**
   * Construct for Laravel validator.
   *
   * @param \Drupal\Core\Extension\ModuleExtensionList $extension_list
   *   The extension list.
   */
  public function __construct(ModuleExtensionList $extension_list) {
    $this->extensionList = $extension_list;
    $loader = new ArrayLoader();
    $defaultMessages = require_once DRUPAL_ROOT . '/' . $this->extensionList->getPath('laravel_helpers') . '/src/validation.php';
    array_walk_recursive($defaultMessages, function (&$message) {
      // @codingStandardsIgnoreStart
      $message = (string) t($message);
      // @codingStandardsIgnoreEnd
    });

    $loader->addMessages('en', 'validation', (array) $defaultMessages);
    $translator = new Translator($loader, 'en');
    $this->validator = new ValidatorFactory($translator);
  }

  /**
   * {@inheritdoc}
   */
  public function validate(array $data, array $rules, array $messages = [], array $attributes = []): array {
    $validator = $this->validator->make($data, $rules, $messages, $attributes);
    return [
      'fails' => $validator->fails(),
      'errors' => $validator->errors()->messages(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function validateFields(array $form, FormStateInterface $form_state): void {
    $data = [];
    $rules = [];
    $messages = [];
    $attributes = [];
    foreach (Element::children($form) as $element_key) {
      // Only execute the validator if a value has been set.
      if (!isset($form[$element_key]['#laravel_validators'])) {
        continue;
      }
      $value = $form_state->getValue($element_key);
      $data[$element_key] = $value;

      if (isset($form[$element_key]['#title'])) {
        $attributes[$element_key] = $form[$element_key]['#title'];
      }

      if (is_string($form[$element_key]['#laravel_validators'])) {
        $rules[$element_key] = $form[$element_key]['#laravel_validators'];
      }

      if (is_array($form[$element_key]['#laravel_validators'])) {
        $element_rules = [];
        foreach ($form[$element_key]['#laravel_validators'] as $constraint_key => $message) {
          if (is_numeric($constraint_key)) {
            if (is_string($message)) {
              $message = explode('|', $message);
              foreach ($message as $item) {
                $element_rules[] = $item;
              }
            }
            else {
              $element_rules[] = $message;
            }
          }
          else {
            if ($constraint_key === '#messages') {
              foreach ($message as $key => $item) {
                $messages[$element_key . '.' . $key] = $item;
              }
            }
            else {
              $element_rules[] = $constraint_key;
              $constraint_key = explode(':', $constraint_key)[0];
              $messages[$element_key . '.' . $constraint_key] = $message;
            }
          }
        }
        $rules[$element_key] = $element_rules;
      }
    }

    $this->execValidate($data, $rules, $messages, $attributes, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array $form, FormStateInterface $form_state): void {
    if (!isset($form['#laravel_form_validators'])) {
      return;
    }
    $data = $form_state->getValues();
    $messages = [];
    $attributes = [];
    $rules = $form['#laravel_form_validators'];
    if (isset($rules['#messages'])) {
      $messages = $rules['#messages'];
      unset($rules['#messages']);
    }

    if (isset($rules['#attributes'])) {
      $attributes = $rules['#attributes'];
      unset($rules['#attributes']);
    }

    $form_attributes = [];
    $data_keys = array_keys($rules);
    foreach ($data_keys as $data_key) {
      $data_keys_arr = explode('.', $data_key);
      $data_keys_arr[] = '#title';
      if ($title = NestedArray::getValue($form, $data_keys_arr)) {
        $form_attributes[$data_key] = $title;
      }
    }
    $attributes = $attributes + $form_attributes;
    $this->execValidate($data, $rules, $messages, $attributes, $form_state);
  }

  /**
   * Exec validate and display form error.
   *
   * @param array $data
   *   The data.
   * @param array $rules
   *   The validate rules.
   * @param array $messages
   *   The messages.
   * @param array $attributes
   *   The attributes.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  protected function execValidate(array $data, array $rules, array $messages, array $attributes, FormStateInterface $form_state): void {
    if (count($rules) === 0) {
      return;
    }

    $validate_results = $this->validate($data, $rules, $messages, $attributes);
    if (!$validate_results['fails']) {
      return;
    }

    foreach ($validate_results['errors'] as $field_name => $field_errors) {
      $error_messages = [];
      foreach ($field_errors as $field_error) {
        $error_messages[] = $field_error;
      }
      $form_state->setErrorByName($field_name, implode(" ", $error_messages));
    }
  }

}
