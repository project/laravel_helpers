<?php

namespace Drupal\laravel_helpers\Service;

use Drupal\Core\Form\FormStateInterface;

/**
 * The Laravel validate interface.
 */
interface ValidatorsInterface {

  /**
   * Validate the data.
   *
   * @param array $data
   *   The array of data.
   * @param array $rules
   *   The array of rules.
   * @param array $messages
   *   The array of messages.
   * @param array $attributes
   *   The array of attributes.
   *
   * @return array
   *   The validator result.
   */
  public function validate(array $data, array $rules, array $messages = [], array $attributes = []): array;

  /**
   * Validate form fields.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function validateFields(array $form, FormStateInterface $form_state): void;

  /**
   * Validate form.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function validateForm(array $form, FormStateInterface $form_state): void;

}
