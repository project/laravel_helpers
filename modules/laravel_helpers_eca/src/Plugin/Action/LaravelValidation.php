<?php

namespace Drupal\laravel_helpers_eca\Plugin\Action;

use Drupal\Core\Form\FormStateInterface;
use Drupal\eca\Plugin\Action\ConfigurableActionBase;
use Drupal\eca\Service\YamlParser;
use Drupal\laravel_helpers\Service\ValidatorsInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Yaml\Exception\ParseException;

/**
 * Action set condition result.
 *
 * @Action(
 *   id = "laravel_helpers_eca_validation",
 *   label = @Translation("Laravel: Validation"),
 *   description = @Translation("Validation data with Laravel helpers.")
 * )
 */
class LaravelValidation extends ConfigurableActionBase {

  /**
   * The YAML parser.
   */
  protected YamlParser $yamlParser;

  /**
   * The Laravel validator.
   */
  protected ValidatorsInterface $validator;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): static {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->yamlParser = $container->get('eca.service.yaml_parser');
    $instance->validator = $container->get('laravel_helpers.validator');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
        'data' => '',
        'data_yaml' => '',
        'rules' => '',
        'token_name' => '',
        'display_error' => '',
      ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['data'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Data'),
      '#description' => $this->t('The data used for validation.'),
      '#default_value' => $this->configuration['data'],
      '#weight' => -50,
      '#eca_token_replacement' => TRUE,
    ];

    $form['data_yaml'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Data in YAML'),
      '#description' => $this->t('Data in the YAML format.'),
      '#required' => FALSE,
      '#return_value' => 1,
      '#weight' => -49,
      '#default_value' => $this->configuration['data_yaml'],
    ];

    $form['rules'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Rules'),
      '#description' => $this->t('The validation rules in yaml format. Supported <a target="_blank" href="https://laravel.com/docs/validation#available-validation-rules">rules</a>.'),
      '#default_value' => $this->configuration['rules'],
      '#weight' => -48,
      '#eca_token_replacement' => TRUE,
    ];

    $form['display_error'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display error messages'),
      '#description' => $this->t('Display error messages when validation fail.'),
      '#required' => FALSE,
      '#return_value' => 1,
      '#weight' => -45,
      '#default_value' => $this->configuration['display_error'],
    ];

    $form['token_name'] = [
      '#required' => TRUE,
      '#type' => 'textfield',
      '#title' => $this->t('Validation result token'),
      '#default_value' => $this->configuration['token_name'],
      '#description' => $this->t('The Validation result will be loaded into this specified token. You can access [token_name:fails], [token_name:errors]'),
      '#eca_token_reference' => TRUE,
      '#weight' => -40,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    $this->configuration['data'] = $form_state->getValue('data');
    $this->configuration['data_yaml'] = $form_state->getValue('data_yaml');
    $this->configuration['rules'] = $form_state->getValue('rules');
    $this->configuration['display_error'] = $form_state->getValue('display_error');
    $this->configuration['token_name'] = $form_state->getValue('token_name');
    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function execute(): void {
    $data = $this->tokenService->getOrReplace($this->configuration['data']);
    $data_yaml = $this->tokenService->getOrReplace($this->configuration['data_yaml']);
    if ($data_yaml) {
      try {
        $data = $this->yamlParser->parse($data);
      }
      catch (ParseException $e) {
        $this->logger->error('Laravel: Validation: Tried parsing data as YAML format, but parsing failed.' . $e->getMessage());
        return;
      }
    }
    if (empty($data)) {
      $data = [];
    }
    $rules = $this->tokenService->getOrReplace($this->configuration['rules']);
    if ($rules) {
      try {
        $rules = $this->yamlParser->parse($rules);
      }
      catch (ParseException $e) {
        $this->logger->error('Laravel: Validation: Tried parsing rules as YAML format, but parsing failed.' . $e->getMessage());
        return;
      }
    }

    if (!$rules) {
      return;
    }

    $messages = [];
    $attributes = [];
    if (isset($rules['#messages'])) {
      $messages = $rules['#messages'];
      unset($rules['#messages']);
    }

    if (isset($rules['#attributes'])) {
      $attributes = $rules['#attributes'];
      unset($rules['#attributes']);
    }

    $result = $this->validator->validate($data, $rules, $messages, $attributes);
    if ($result['fails'] && !empty($this->configuration['display_error'])) {
      foreach ($result['errors'] as $field_name => $field_errors) {
        $error_messages = [];
        foreach ($field_errors as $field_error) {
          $error_messages[] = $field_error;
        }
        $this->messenger()->addError(implode(" ", $error_messages));
      }
    }
    if (!empty($this->configuration['token_name'])) {
      $this->tokenService->addTokenData($this->configuration['token_name'], $result);
    }
  }

}
