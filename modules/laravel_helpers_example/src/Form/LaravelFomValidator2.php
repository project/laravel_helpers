<?php

namespace Drupal\laravel_helpers_example\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Example Laravel Form Validator.
 */
class LaravelFomValidator2 extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'laravel_fom_validator2';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['name'] = [
      '#type' => 'textfield',
      '#required' => FALSE,
      '#title' => $this->t('Name'),
      '#description' => $this->t('Input your name, ascii with number. Exclude aaa'),
    ];

    // Use custom validation class.
    $form['nick_name'] = [
      '#type' => 'textfield',
      '#required' => FALSE,
      '#title' => $this->t('Nick Name'),
      '#description' => $this->t('Input your nick name, it required uppercase'),
    ];

    $form['email'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Email'),
      '#description' => $this->t('Input your email.'),
    ];

    $form['age'] = [
      '#type' => 'textfield',
      '#required' => FALSE,
      '#title' => $this->t('Age'),
      '#description' => $this->t('Input your age. Min 18, Max 50'),
    ];

    $form['favorite'] = [
      '#type' => 'textfield',
      '#required' => FALSE,
      '#title' => $this->t('Favorite'),
      '#description' => $this->t('Input your favorite. It required when age = 18 or age > 30'),
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];

    $form['#laravel_form_validators'] = [
      'name' => 'required|alpha_num:ascii',
      'nick_name' => 'required|string|uppercase',
      'email' => 'email|max:50',
      'age' => 'required|numeric|min:18|max:50',
      'favorite' => 'required_if:age,18',
      '#messages' => [
        'email.email' => ':attribute require valid email address',
        'favorite.required_if' => ':attribute need input when age = 18',
      ],
      '#attributes' => [
        'favorite' => 'Your Favorite',
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->messenger()
      ->addMessage($this->t('Your form was submitted successfully.'));
  }

}
