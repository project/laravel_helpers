<?php

namespace Drupal\laravel_helpers_example\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\laravel_helpers_example\ValidationRule\Uppercase;
use Illuminate\Validation\Rules\RequiredIf;

/**
 * Example Laravel Form Validator.
 */
class LaravelFomValidator extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'laravel_fom_validator';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['name'] = [
      '#type' => 'textfield',
      '#required' => FALSE,
      '#title' => $this->t('Name'),
      '#description' => $this->t('Input your name, ascii with number. Exclude aaa'),
      '#laravel_validators' => [
        'required',
        'alpha_num:ascii',
        function (string $attribute, mixed $value, \Closure $fail) use ($form_state) {
          if ($value === 'aaa') {
            $fail("The :attribute is invalid.");
          }
        },
      ],
    ];

    // Use custom validation class.
    $form['nick_name'] = [
      '#type' => 'textfield',
      '#required' => FALSE,
      '#title' => $this->t('Nick Name'),
      '#description' => $this->t('Input your nick name, it required uppercase'),
      '#laravel_validators' => [
        'required',
        'string',
        new Uppercase(),
      ],
    ];

    $form['email'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Email'),
      '#description' => $this->t('Input your email.'),
      '#laravel_validators' => [
        'email' => $this->t(':attribute require valid email address'),
        'max:50',
      ],
    ];

    $form['age'] = [
      '#type' => 'textfield',
      '#required' => FALSE,
      '#title' => $this->t('Age'),
      '#description' => $this->t('Input your age. Min 18, Max 50'),
      '#laravel_validators' => 'required|numeric|min:18|max:50',
    ];

    $form['favorite'] = [
      '#type' => 'textfield',
      '#required' => FALSE,
      '#title' => $this->t('Favorite'),
      '#description' => $this->t('Input your favorite. It required when age = 18 or age > 30'),
      '#laravel_validators' => [
        new RequiredIf(function () use ($form, $form_state) {
          $age = $form_state->getUserInput()['age'];
          return $age && (intval($age) > 30 || intval($age) == 18);
        }),
        '#messages' => [
          'required' => ':attribute is required when age = 18 or age > 30',
        ],
      ],
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->messenger()
      ->addMessage($this->t('Your form was submitted successfully.'));
  }

}
