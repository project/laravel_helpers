<?php

namespace Drupal\laravel_helpers_example\ValidationRule;

use Illuminate\Contracts\Validation\ValidationRule;

/**
 * Custom uppercase validation rules.
 */
class Uppercase implements ValidationRule {

  /**
   * Run the validation rule.
   */
  public function validate(string $attribute, mixed $value, \Closure $fail): void {
    if (strtoupper($value) !== $value) {
      $fail('The :attribute must be uppercase.');
    }
  }

}
